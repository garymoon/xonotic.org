---
author: admin
comments: false
date: 2010-05-30 15:09:26+00:00
type: page
slug: team
title: Team Xonotic
wordpress_id: 97
---

## Core Team

The Core team consists of members who discuss and vote on large project changes. This international group of leaders is a classic group of collaborators who above all else, will try and do what's best for the game and community. You can find most of them on our [Freenode channel](https://webchat.freenode.net/?channels=xonotic).

  * [divVerent](http://forums.xonotic.org/member.php?action=profile&uid=4)
  * [-z-](http://forums.xonotic.org/member.php?action=profile&uid=1)
  * [Merlijn Hofstra](http://forums.xonotic.org/member.php?action=profile&uid=34)
  * [Peter Pielak ("Morphed")](http://forums.xonotic.org/member.php?action=profile&uid=8)
  * [Samual](http://forums.xonotic.org/member.php?action=profile&uid=164)
  * [Antibody](http://forums.xonotic.org/member.php?action=profile&uid=530)
  * [CuBe0wL](http://forums.xonotic.org/member.php?action=profile&uid=15)
  * [Mirio](http://forums.xonotic.org/member.php?action=profile&uid=213)
  * [Mario](http://forums.xonotic.org/member.php?action=profile&uid=1258)

### Extended Team

  * [Archer](http://forums.xonotic.org/member.php?action=profile&uid=3351)
  * [bitbomb](http://forums.xonotic.org/member.php?action=profile&uid=446)
  * [Debugger](http://forums.xonotic.org/member.php?action=profile&uid=222)
  * [FruitieX](http://forums.xonotic.org/member.php?action=profile&uid=29)
  * [GATTS](http://forums.xonotic.org/member.php?action=profile&uid=4680)
  * [Halogene](http://forums.xonotic.org/member.php?action=profile&uid=53)
  * [IDWMaster](http://forums.xonotic.org/member.php?action=profile&uid=574)
  * [JH0nny](http://forums.xonotic.org/member.php?action=profile&uid=4193)
  * [kuniuthefrogg](http://forums.xonotic.org/member.php?action=profile&uid=184)
  * [matthiaskrgr](http://forums.xonotic.org/member.php?action=profile&uid=2969)
  * [MrBougo](http://forums.xonotic.org/member.php?action=profile&uid=30)
  * [nilyt](http://forums.xonotic.org/member.php?action=profile&uid=842)
  * [Nitroxis](http://forums.xonotic.org/member.php?action=profile&uid=1003)
  * [packer](http://forums.xonotic.org/member.php?action=profile&uid=373)
  * [s1lence](http://forums.xonotic.org/member.php?action=profile&uid=1879)
  * [sev](http://forums.xonotic.org/member.php?action=profile&uid=46)
  * [Soelen](http://forums.xonotic.org/member.php?action=profile&uid=813)
  * Sydes
  * [terencehill](http://forums.xonotic.org/member.php?action=profile&uid=620)
  * [theShadow](http://forums.xonotic.org/member.php?action=profile&uid=153)
  * [unfa](http://forums.xonotic.org/member.php?action=profile&uid=234)
  * [zykure](http://forums.xonotic.org/member.php?action=profile&uid=3578)


Follow the [RSS Feed](http://xonpickbot.designxenon.com:27500/feed) to read the latest decisions in development.

### List of Coordinators

Coordinators are here to guide contributors, provide information both in and outside their area of expertise and act as liasons to the core team to ensure the most important information doesn't go overlooked. Coordinators will be the people to ask questions to regarding their subjects. If you're feeling lost, contact these people, their names are linked to their forum accounts.

#### Artwork

  * [Diabolik](http://forums.xonotic.org/member.php?action=profile&uid=14)
  * [Peter Pielak ("Morphed")](http://forums.xonotic.org/member.php?action=profile&uid=8)
  * [FruitieX](http://forums.xonotic.org/member.php?action=profile&uid=29)

#### Website

  * [Antibody (web)](http://forums.xonotic.org/member.php?action=profile&uid=530)
  * [-z- (web / game)](http://forums.xonotic.org/member.php?action=profile&uid=1)
  * [merlijn](http://forums.xonotic.org/member.php?action=profile&uid=34)

#### Level Design

  * [FruitieX](http://forums.xonotic.org/member.php?action=profile&uid=29)

#### Music / Sound FX

  * [mand1nga](http://forums.xonotic.org/member.php?action=profile&uid=13)
  * [merlijn](http://forums.xonotic.org/member.php?action=profile&uid=34)

#### Engine Code

  * [divVerent](http://forums.xonotic.org/member.php?action=profile&uid=4)

#### Game Code

  * [divVerent](http://forums.xonotic.org/member.php?action=profile&uid=4)
  * [FruitieX](http://forums.xonotic.org/member.php?action=profile&uid=29)
  * [tZork](http://forums.xonotic.org/member.php?action=profile&uid=39)
  * [Samual](http://forums.xonotic.org/member.php?action=profile&uid=164)

#### Marketing / PR

  * [-z-](http://forums.xonotic.org/member.php?action=profile&uid=1)
  * [mand1nga](http://forums.xonotic.org/member.php?action=profile&uid=13)
  * [Samual](http://forums.xonotic.org/member.php?action=profile&uid=164)

#### Legal

  * [divVerent](http://forums.xonotic.org/member.php?action=profile&uid=4)
  * [merlijn](http://forums.xonotic.org/member.php?action=profile&uid=34)

#### Stats

  * [Antibody](http://forums.xonotic.org/member.php?action=profile&uid=530)
  * [zykure](http://forums.xonotic.org/member.php?action=profile&uid=3578)

**Note:** Many of the core members are serving double roles as coordinators. This is not a considered a conflict of interests in decision making because these members are largely filling the role until someone else who fits the description can fill it.

These coordination positions are time-consuming roles that require dedication, knowledge of the area and sharpened communication skills. If you believe you can be of assistance in this area, you may contact the team at [admin@xonotic.org](mailto:admin@xonotic.org) or in [FreeNode #xonotic](https://webchat.freenode.net/?channels=xonotic). Don't forget, you can still contribute in other ways without the responsibilities of a coordinator.

### Other Contributors and Supporters

See the Credits in the game for the most up-to-date list of contributors.

Please view our [FAQ page](/faq) for more information.
